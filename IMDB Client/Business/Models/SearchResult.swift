//
//  SearchResult.swift
//  IMDB Client
//
//  Created by mac on 28.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchResult: Mappable {
    required init?(map: Map) { }
    
    var search: [SearchResultItem]!
    var totalResults: Int!
    var response: Bool!
    
    private let searchKey = "Search"
    private let totalResultsKey = "totalResults"
    private let responseKey = "Response"
    
    func mapping(map: Map) {
        search <- map[searchKey]
        totalResults <- (map[totalResultsKey], Transformations.Integer)
        response <- (map[responseKey], Transformations.Bool)
    }
}

class SearchResultItem: Mappable {
    required init?(map: Map) { }
    
    var title: String!
    var year: String!
    var id: String!
    var type: String!
    var posterUrl: String!
    
    private let titleKey = "Title"
    private let yearKey = "Year"
    private let idKey = "imdbID"
    private let typeKey = "Type"
    private let posterUrlKey = "Poster"
    
    func mapping(map: Map) {
        title <- map[titleKey]
        year <- map[yearKey]
        id <- map[idKey]
        type <- map[typeKey]
        posterUrl <- map[posterUrlKey]
    }
}
