//
//  FilmDetails.swift
//  IMDB Client
//
//  Created by mac on 29.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import ObjectMapper

class FilmDetails: Mappable {
    var posterUrl: URL?
    var title: String!
    var year: String!
    var rating: Double!
    var country: String!
    var producer: String!
    var actors: String!
    var description: String!
    var response: Bool!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        title <- map["Title"]
        year <- map["Year"]
        rating <- (map["imdbRating"], Transformations.Dbl)
        country <- map["Country"]
        producer <- map["Director"]
        actors <- map["Actors"]
        description <- map["Plot"]
        posterUrl <- (map["Poster"], Transformations.Url)
        response <- (map["Response"], Transformations.Bool)
    }
}
