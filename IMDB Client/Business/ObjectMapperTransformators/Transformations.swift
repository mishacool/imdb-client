//
//  Transforms.swift
//  IMDB Client
//
//  Created by mac on 29.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import ObjectMapper

struct Transformations {
    static let Integer = TransformOf<Int, String>(fromJSON: { (value: String?) -> Int? in
        return Int(value!)
    }, toJSON: { (value: Int?) -> String? in
        if let value = value {
            return String(value)
        }
        return nil
    })
    
    static let Bool = TransformOf<Bool, String>(fromJSON: { (value: String?) -> Bool? in
        return value == "True"
    }) { (value: Bool?) -> String? in
        if let value = value {
            return value ? "True" : "False"
        }
        return nil
    }
    
    static let Dbl = TransformOf<Double, String>(fromJSON: { (value: String?) -> Double? in
        return Double(value!)
    }) { (value: Double?) -> String? in
        if let value = value {
            return String(value)
        }
        return nil
    }
    
    static let Url = TransformOf<URL, String>(fromJSON: { (value: String?) -> URL? in
        if let value = value {
            return URL(string: value)
        } else {
            return nil
        }
    }) { (value: URL?) -> String? in
        if let value = value {
            return "\(value)"
        }
        return nil
    }
}
