//
//  NetworkFactory.swift
//  IMDB Client
//
//  Created by mac on 28.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

struct NetworkParams {
    static var baseUrl: String = "http://www.omdbapi.com/"
    static var apiKey: String = "26a61db0"
}

class NetworkFactory {
    static func loadFilmsPreview(search: String, page: Int, onLoaded: ((_ data: SearchResult) -> Void)?, onError: (() -> Void)?) {
        // encodeUrl - для правильной кодировки кириллических символов, но перед этим меняем все _ на +,
        // иначе они тоже заменятся с процентами, а кинопоиску этого не надо
        let url = "\(NetworkParams.baseUrl)?apikey=\(NetworkParams.apiKey)&s=\(search)&page=\(page)".replacingOccurrences(of: " ", with: "+").encodeUrl()!
        Alamofire.request(url)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseObject { (response: DataResponse<SearchResult>) in
                guard let data = response.result.value else {
                    print("error parsing")
                    return
                }
                if data.response {
                    onLoaded?(data)
                } else {
                    onError?()
                }
        }
    }
    
    static func loadFilmDetails(id: String, onLoaded: ((_ data: FilmDetails) -> Void)?, onError: (() -> Void)?) {
        let url = "\(NetworkParams.baseUrl)?apikey=\(NetworkParams.apiKey)&i=\(id)"
        Alamofire.request(url)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseObject { (response: DataResponse<FilmDetails>) in
                guard let data = response.result.value else {
                    print("error parsing")
                    return
                }
                if data.response {
                    onLoaded?(data)
                } else {
                    onError?()
                }
        }
    }
}
