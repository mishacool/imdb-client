//
//  SearchFilmModel.swift
//  IMDB Client
//
//  Created by mac on 28.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation

struct SearchFilmModel {
    var id: String
    var title: String
    var year: String
}
