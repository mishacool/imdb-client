//
//  SearchViewModel.swift
//  IMDB Client
//
//  Created by mac on 28.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation

class SearchViewModel {
    private var currentPage: Int = 0
    private var currentSearch: String?
    private var currentTotalCount: Int = 0
    var data: [SearchFilmModel] = []
    var allItemsLoaded: Bool {
        return data.count == currentTotalCount
    }
    
    func loadFilmsNextPage(onLoaded: (() -> Void)?) {
        guard let search = currentSearch else {
            return
        }
        NetworkFactory.loadFilmsPreview(search: search, page: currentPage + 1, onLoaded: { [unowned self] data in
            self.currentTotalCount = data.totalResults
            self.data.append(contentsOf: data.search.map { SearchFilmModel(id: $0.id, title: $0.title, year: $0.year) })
            onLoaded?()
            self.currentPage += 1
        }, onError: nil)
    }
    
    func newSearch(search: String, onLoaded: (() -> Void)?) {
        currentPage = 1
        currentSearch = search
        NetworkFactory.loadFilmsPreview(search: search, page: currentPage, onLoaded: { [unowned self] data in
            self.currentTotalCount = data.totalResults
            self.data = data.search.map { SearchFilmModel(id: $0.id, title: $0.title, year: $0.year) }
            onLoaded?()
        }) {
            self.currentTotalCount = 0
            self.data = []
            onLoaded?()
        }
    }
}
