//
//  SearchViewController.swift
//  IMDB Client
//
//  Created by mac on 28.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    private var viewModel: SearchViewModel!
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        viewModel = SearchViewModel()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filmDetails" {
            guard let vc = segue.destination as? FilmDetailsViewController,
                let data = sender as? SearchFilmModel else {
                return
            }
            vc.filmId = data.id
            vc.navigationItem.title = data.title
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.newSearch(search: searchText) { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = "\(viewModel.data[indexPath.row].title) (\(viewModel.data[indexPath.row].year))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "filmDetails", sender: viewModel.data[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // вычитаемое число определяет опережение, с которым подгружать новые превьюшки
        let lastElement = viewModel.data.count - 10
        if indexPath.row == lastElement && !viewModel.allItemsLoaded {
            self.viewModel.loadFilmsNextPage { [unowned self] in
                self.tableView.reloadData()
            }
        }
    }
}
