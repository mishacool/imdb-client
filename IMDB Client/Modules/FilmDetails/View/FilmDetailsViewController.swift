//
//  FilmDetailsViewController.swift
//  IMDB Client
//
//  Created by mac on 29.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import SDWebImage

class FilmDetailsViewController: UIViewController {
    private var viewModel: FilmDetailsViewModel!
    var filmId: String!

    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var actorsLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var countryLabel: UILabel!
    @IBOutlet private weak var producerLabel: UILabel!
    @IBOutlet private weak var ratingView: UIView!
    private var ratingBackground: CAShapeLayer!
    private var ratingStarsSize: CGSize!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRatingView()

        viewModel = FilmDetailsViewModel()
        viewModel.loadFilmDetails(id: filmId) { [unowned self] in
            let data = self.viewModel.data
            
            self.titleLabel.text = "\(data?.title ?? "") (\(data?.year ?? ""))"
            
            self.posterImageView.sd_setImage(with: data?.posterUrl, placeholderImage: #imageLiteral(resourceName: "posterPlaceholder"), options: [], completed: nil)
            
            self.countryLabel.text = data?.country
            self.producerLabel.text = data?.producer
            self.descriptionLabel.text = data?.description
            self.actorsLabel.text = data?.actors
            if let rating = data?.rating {
                self.animateRating(rating: rating)
            }
        }
    }
    
    private func animateRating(rating: Double) {
        let destinationPath = UIBezierPath(rect: CGRect(origin: .zero, size: CGSize(width: (CGFloat(rating) / 10) * ratingStarsSize.width, height: ratingStarsSize.height))).cgPath
        ratingBackground.path = destinationPath
        let widthAnimation = CABasicAnimation(keyPath: "path")
        widthAnimation.fromValue = UIBezierPath(rect: CGRect(origin: .zero, size: CGSize(width: 0, height: ratingStarsSize.height))).cgPath
        widthAnimation.toValue = destinationPath
        widthAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        widthAnimation.duration = 1
        ratingBackground.add(widthAnimation, forKey: nil)
        
    }
    
    private func configureRatingView() {
        ratingView.layoutIfNeeded()
        let starsCount = 5
        let sideLength = ratingView.bounds.height
        let starsSpacing: CGFloat = sideLength + 5
        let starSize = CGSize(width: sideLength, height: sideLength)
        let test = starsSpacing * CGFloat(starsCount) - 5
        ratingStarsSize = CGSize(width: test, height: sideLength)
        
        let starShape = UIBezierPath()
        //рисуем звезды
        for i in 0...(starsCount - 1) {
            let multiplier = CGFloat(i)
            starShape.move(to: CGPoint(x: starSize.width * 0.5 + starsSpacing * multiplier, y: 0))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.675 + starsSpacing * multiplier, y: starSize.height * 0.25))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.98 + starsSpacing * multiplier, y: starSize.height * 0.35))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.795 + starsSpacing * multiplier, y: starSize.height * 0.6))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.79 + starsSpacing * multiplier, y: starSize.height * 0.9))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.5 + starsSpacing * multiplier, y: starSize.height * 0.8))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.21 + starsSpacing * multiplier, y: starSize.height * 0.9))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.205 + starsSpacing * multiplier, y: starSize.height * 0.6))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.02 + starsSpacing * multiplier, y: starSize.height * 0.35))
            starShape.addLine(to: CGPoint(x: starSize.width * 0.325 + starsSpacing * multiplier, y: starSize.height * 0.25))
            starShape.addLine(to: CGPoint(x: starSize.width / 2 + starsSpacing * multiplier, y: 0))
        }
        
        ratingBackground = CAShapeLayer()
        ratingBackground.path = UIBezierPath(rect: CGRect(origin: .zero, size: CGSize(width: 0, height: starSize.height))).cgPath
        ratingBackground.fillColor = UIColor.yellow.cgColor
        ratingBackground.frame = ratingView.bounds
        
        let starsLayerBorder = CAShapeLayer()
        starsLayerBorder.path = starShape.cgPath
        starsLayerBorder.strokeColor = UIColor.lightGray.cgColor
        starsLayerBorder.lineWidth = 0.5
        starsLayerBorder.fillColor = UIColor.clear.cgColor
        starsLayerBorder.frame = ratingView.bounds
        
        let starsMask = CAShapeLayer()
        starsMask.path = starShape.cgPath
        starsMask.frame = ratingView.bounds
        
        ratingView.layer.addSublayer(ratingBackground)
        ratingView.layer.addSublayer(starsLayerBorder)
        ratingView.layer.mask = starsMask
    }
}
