//
//  FilmDetailsViewModel.swift
//  IMDB Client
//
//  Created by mac on 29.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation

class FilmDetailsViewModel {
    var data: FilmDetails?
    
    func loadFilmDetails(id: String, onLoaded: (() -> Void)?) {
        NetworkFactory.loadFilmDetails(id: id, onLoaded: { data in
            self.data = data
            onLoaded?()
        }) {
            print("error loading")
        }
    }
}
