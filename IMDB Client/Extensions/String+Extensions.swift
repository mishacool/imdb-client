//
//  String+Extensions.swift
//  IMDB Client
//
//  Created by mac on 28.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation

extension String
{
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
}
