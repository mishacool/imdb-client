//
//  UILabel+Extensions.swift
//  IMDB Client
//
//  Created by mac on 29.07.2018.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    /**
     Устанавливает части текста определенный шрифт (текст уже должен быть установлен к этому моменту)
     - parameters:
     - for: Часть текст, для которой необходимо установить шрифт
     - font: Шрифт, который необходимо установить (по умолчанию, если параметр не усатновлен, берет текущий шрифт и делает его medium)
     */
    func setFont(for textPart: String, font: UIFont? = nil) {
        guard let text = self.text else {
            return
        }
        let myMutableString = NSMutableAttributedString(string: text)
        myMutableString.addAttribute(
            NSAttributedStringKey.font,
            value: font ?? UIFont.boldSystemFont(ofSize: self.font.pointSize),
            range: (text as NSString).range(of: textPart))
        self.attributedText = myMutableString
    }
}
